<?php

namespace App\Permissions;

use App\{Role, Permission};


trait HasPermissionsTrait{

    public function givePermissionTo(...$permission){

        $permission = $this->getAllPermissions(array_flatten($permission));

        if($permission === null){

            return $this;
        }
        
        $this->permissions()->saveMany($permission);

        return $this;
    }

    public function withdrawPermissionsTo(...$permissions){

        $permissions = $this->getAllPermissions(array_flatten($permissions));

        $this->permissions()->detach($permissions);

        return $this;
    }

    public function updatePermissionsTo(...$permissions){

        $this->permissions()->detach();

        $this->givePermissionTo($permissions);

    
    }


     public function hasRole(...$roles){

        //dd($roles); 

        foreach($roles as $role){

            if($this->roles->contains('role_name', $role)){

                return true;
            }
        } 

        return false;
         
    }

    public function hasPermissionThroughRole($permission){

        foreach($permission->roles as $role){

            if($this->roles->contains($role));
             return true;
        }

        return false;

    }

    protected function getAllPermissions($permission){

        return Permission::WhereIn('permission_name', $permission)->get();
    }


    public function hasPermission($permission){

        //eventually has permission through role
        return (bool) $this->permissions->where('permission_name', $permission->permission_name)->count();
        
    }

    public function hasPermissionTo($permission){

        return $this->hasPermission($permission) || $this->hasPermissionThroughRole($permission);
    }

   
    public function roles(){

        return $this->belongsToMany(Role::class, 'users_roles');
    }

    public function permissions(){

        return $this->belongsToMany(Permission::class, 'users_permissions');
    }
}
