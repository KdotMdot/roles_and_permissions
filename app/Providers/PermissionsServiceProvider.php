<?php

namespace App\Providers;

use Gate;
use App\Permission;
use Illuminate\Support\ServiceProvider;

class PermissionsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Permission::get()->map(function ($permission) {

            Gate::define($permission->permission_name, function ($user) use ($permission) {

                 return $user->hasPermissionTo($permission);

            });
        });
    }

    public function register(){


    }

}

