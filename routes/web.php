<?php

use Illuminate\Http\Request;

Route::get('/', 'UsersController@hasRole');

Route::get('permission', function(Request $request){

    $user = $request->user();
    dump($request->method());
    dump($user->can('delete post'));

});

Route::get('give', function(Request $request){

    $user = $request->user();
    $user->givePermissionTo(['delete post', 'edit post', 'create post']);

});
Route::get('take', function(Request $request){

    $user = $request->user();
    $user->withdrawPermissionsTo(['delete post', 'edit post', 'create post']);

});

Route::get('update', function(Request $request){

    $user = $request->user();
    $user->updatePermissionsTo(['delete post', 'create post']);

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
